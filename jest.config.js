module.exports = {
    preset: 'jest-preset-angular',
    testMatch: ['<rootDir>/src/**/*.spec.ts'],
    testPathIgnorePatterns: [
      '<rootDir>/node_modules/',
    ],
  };