import { FormComponent } from './form.component';
import { render } from '@testing-library/angular';
import { ReactiveFormsModule } from '@angular/forms';

describe('FormComponent', () => {
  it('should render the form', async () => {
    await render(FormComponent, { imports: [ReactiveFormsModule] });
  });
});
