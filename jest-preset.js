module.exports = {
  preset: '@testing-library/angular',
  setupFilesAfterEnv: ['@testing-library/jest-dom'],
};